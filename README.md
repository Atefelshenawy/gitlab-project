# develop Java Application

This is a simple Java application that prints "Hello, World!" to the console.

## Push your code to Gitlap

Clone the repository locally
 ```sh
 git clone <your-gitlab-repo-url>
 cd <your-repo-name>

## Copy your Java project files into the repository

 ```sh
cp -r path/to/your/my-java-app/* .

## Add and commit the files:

   ```sh
 git add .
 git commit -m "Initial commit"
 git push origin main

## Create and push the develop branch

   ```sh
 git checkout -b develop
 git push origin develop

 ## Create and push the master branch:

     ```sh
   git checkout -b master
   git push origin master 

 ##   Step 5: Protect the Master Branch
 
Go to your GitLab project settings:
Navigate to Settings > Repository.
Under Protected Branches, find the master branch.
Set it as a protected branch, allowing only Maintainers and Owners to push and merge












  
