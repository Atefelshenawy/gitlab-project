# CI/CD Pipeline for My Java Application

This CI/CD pipeline is set up using GitLab CI/CD to build, test, and deploy the application to AWS across three environments: Dev, Test, and STG.

## Pipeline Stages

- **Build:** Compiles the Java application.
- **Test:** Runs the unit tests.
- **Deploy to Dev:** Deploys the application to the Dev environment.
- **Deploy to Test:** Deploys the application to the Test environment after successful deployment to Dev.
- **Deploy to STG:** Deploys the application to the STG environment when changes are merged into the `master` branch.

## Protected Branches

The `master` branch is protected, allowing only `Maintainers` and `Owners` to merge changes.

## How to Trigger Deployments

- **Dev Environment:** Any push to the `develop` branch.
- **Test Environment:** Successful deployment to the Dev environment.
- **STG Environment:** Merge request to the `master` branch.


  
